package samgotemplate

import (
	"fmt"
	"reflect"
	"testing"
)

func TestBuildStructure(t *testing.T) {
	//"~" tells the template to not render a variable
	template := `
	{{$val1:=random 1 3}}
	{{$val2:=22}}
	{{$val3:=22}}
	{{if (isprime $val1 22) AND ( $op13  >=  $op22  )}}
		{{$val:=34}}
		{{if $op13  >=  $op22 AND $op13  >=  $op22}}
			{{$val:=34}}
		{{end}}
	{{end}}
	{{for (isprime $val1 22) AND ( $op13  >=  $op22  )}}
		{{$val:=34}}
		{{if isprime$val1}}
			{{$val:=34}}
		{{end}}
	{{end}}
	{{$val:=34}}`
	groupMap := map[string]int{}
	for i, v := range baseRegx.SubexpNames() {
		groupMap[v] = i
	}
	indexes := baseRegx.FindAllStringIndex(template, -1)
	matches := baseRegx.FindAllStringSubmatch(template, -1)
	//build the structure
	structures, _ := buildStructure(indexes, matches, groupMap)

	if len(structures) != 6 {
		t.Fatalf("expected 6 structures but found %v", len(structures))
		return
	}
	t.Logf("%v run successful", t.Name())
}

func TestConditionStructure(t *testing.T) {
	template := `
	{{$b := (random 2 15)}}
	{{$a := (random $b 15)}} 
	{{for !(divisible $a $b) OR $a == $b}}
		{{$b := (random 2 15)}}
		{{$a := (random $b 15)}}
	{{end}}
	Hello may i know you?
	a={{$a}} b={{$b}}
	{{if $a > $b}}
		{{$a}} is greater than {{$b}}
	{{end}}`
	// template := `{{$b := (random 1 10)}}{{$a := (random $b 20)}}{{$a}}{{$b}}`
	PrevousVals := []map[string]interface{}{}
	//try multi times
	for i := 0; i < 10; i++ {
		eng := NewEngine(template, nil)
		parsed, err := eng.Execute()
		if err != nil {
			t.Fatalf(t.Name() + " failed: template processing failed")
			return
		}
		variables := (*eng.globals)
		if i != 0 {
			retry := false
			for _, prv := range PrevousVals {
				retry = retry || reflect.DeepEqual(prv, variables)
			}
			if retry {
				i--
				continue
			}
		}
		t.Logf("%v try %d", t.Name(), i)
		fmt.Println(variables)
		fmt.Println(parsed)
		PrevousVals = append(PrevousVals, variables)
	}
	t.Logf("%v run successful", t.Name())
}
