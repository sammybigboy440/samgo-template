package samgotemplate

import (
	"fmt"
	"math"
	"math/rand"
	"regexp"
	"strconv"
	"strings"
)

var (
	//base regex evaluation
	baseRegx = regexp.MustCompile(`(?U)(?P<block>{{(?P<type>[ ?]*if|for) (?P<condition>.+)}})|(?P<declarer>{{[ ]*\$(?P<name>[a-z1-9]+) *(?P<operator>[\:?]*[\=]+) *(?P<value>[.\S ]+)[ ?]*}})|(?P<end>{{end}})|(?P<show>{{[ ]*\$(?P<vname>[a-z1-9]+) *}})`)
	//regex for identifying for and if function evaluator
	ifforRegx = regexp.MustCompile(`(?P<pair>(?P<negate1>[\!]?)[\(]? *(?P<operandl>[\$]?[.\S]+) *((?P<compare>(?:[=]{2})|(?:eq)|(?:\!\=)|(?:neq)|(?:>=)|(?:gteq)|(?:<=)|(?:lseq)|>|(?:gt)|<|(?:ls))) *(?P<operandr>[\$]?[^\s\)]+ *)[\)]?)+ *(?P<operator1>[\&]{2}|(?:AND)|[\|]{2}|(?:OR)|)| *(?P<function>(?P<negate2>[\!]?)[\(]?(?P<name>[^\s\(\)]{2,})(?P<params>[\$? \w]*\b)[\)]?) *(?P<operator2> *[\&]{2}|(?:AND)|[\|]{2}|(?:OR)|)`)
	//regex for identifying variable assignment for either variable or function
	varFuncSliceRegx = regexp.MustCompile(`(?P<variable>[\$]*(?P<vname>[a-zA_Z0-9]+))|(?P<function>[\(]?(?P<fname>[^\s\[]{2,})(?P<params>[\$? \w]*\b)[\)]?)|(?P<slice>(\[)(?P<svalue>(?:[\,]? *[.\S]* *)*)(\]))`)
	//regex maps
	baseRegxMap         = mapRegxNames(baseRegx.SubexpNames())
	ifforRegxMap        = mapRegxNames(ifforRegx.SubexpNames())
	varFuncSliceRegxMap = mapRegxNames(varFuncSliceRegx.SubexpNames())
)

type progTemplate struct {
	globals    *map[string]interface{}
	template   string
	structures []templateStructure
}

func NewEngine(template string, globals *map[string]interface{}) (p progTemplate) {
	if globals == nil {
		globals = &map[string]interface{}{}
	}
	indexes := baseRegx.FindAllStringIndex(template, -1)
	baseRegxMatches := baseRegx.FindAllStringSubmatch(template, -1)
	//structure the template
	s, _ := buildStructure(indexes, baseRegxMatches, baseRegxMap)
	return progTemplate{globals, template, s}
}

func (h *progTemplate) SetTemplate(template string) {
	indexes := baseRegx.FindAllStringIndex(template, -1)
	baseRegxMatches := baseRegx.FindAllStringSubmatch(template, -1)
	h.template = template
	//structure the template
	h.structures, _ = buildStructure(indexes, baseRegxMatches, baseRegxMap)
}

func (h *progTemplate) GetGlobals() *map[string]interface{} {
	return h.globals
}

func (h *progTemplate) SetGlobals(g *map[string]interface{}) {
	h.globals = g
}

type templateStructure struct {
	index    []int //the location in the string
	match    []string
	children []templateStructure
}

func buildStructure(indexes [][]int, matches [][]string, groupMap map[string]int) (structures []templateStructure, position int) {
	for i := 0; i < len(indexes); i++ {
		index := indexes[i]
		match := matches[i]
		stct := templateStructure{index, match, []templateStructure{}}
		if match[groupMap["block"]] != "" { //it is a block
			if i+1 < len(indexes) {
				p := 0
				stct.children, p = buildStructure(indexes[i+1:], matches[i+1:], groupMap)
				i += p
			}
		}
		structures = append(structures, stct)
		if match[groupMap["end"]] != "" { //if its an end tag return to parent
			return structures, i + 1
		}
	}
	return structures, len(indexes) + 1
}

/**
**function for eveluating if or for conditions
**/
func (h *progTemplate) validateCondition(matches [][]string, groupMap map[string]int) (total bool, err error) {
	for i := 0; i < len(matches); i++ {
		//previous index

		var (
			current      bool
			pi           = int(math.Max(float64(i-1), 0))
			joinOperator = (matches[pi][groupMap["operator1"]] + matches[pi][groupMap["operator2"]])
		)
		/**
		** nested condition yet to be implemented
		** To be done here
		**/

		//if the previous is FALSE and the join opertor is AND, return false
		if i > 0 && total && joinOperator == "AND" {
			total = current
			continue
		}

		//run validation
		if matches[i][groupMap["pair"]] != "" { //if it is a validation pair
			//parse as int for now, will be changed later
			left, _ := strconv.Atoi(fmt.Sprint(parseParams(h.globals, matches[i][groupMap["operandl"]])[0]))
			right, _ := strconv.Atoi(fmt.Sprint(parseParams(h.globals, matches[i][groupMap["operandr"]])[0]))
			//process the pair
			//(?:[=]{2})|(?:eq)|(?:>=)|(?:gteq)|(?:<=)|(?:lseq)|>|(?:gt)|<|(?:ls))
			switch matches[i][groupMap["compare"]] {
			case "==", "eq":
				current = left == right
			case "!=", "neq":
				current = left != right
			case ">=", "geq":
				current = left >= right
			case "<=", "leq":
				current = left <= right
			case ">", "gt":
				current = left > right
			case "<", "ls":
				current = left < right
			}
		}
		if matches[i][groupMap["function"]] != "" { //if it is a validation function
			var (
				bind func(params ...interface{}) interface{}
				ok   bool
			)
			//check if the binder exists
			if bind, ok = binds(nil)[matches[i][groupMap["name"]]]; !ok {
				return total, fmt.Errorf("invalid for loop evaluator")
			}
			//get the parameters and convert it to a slice of interface{}
			_p := strings.Split(strings.TrimSpace(matches[i][groupMap["params"]]), " ")
			params := make([]interface{}, len(_p))
			for i, p := range _p {
				params[i] = p
			}
			current, err = strconv.ParseBool(fmt.Sprint(bind(parseParams(h.globals, params...)...)))
			if err != nil {
				return total, err
			}
		}
		if (matches[i][groupMap["negate1"]] + matches[i][groupMap["negate2"]]) != "" {
			current = !current
		}
		if i == 0 { //if the first item
			total = current
			continue
		}
		switch joinOperator {
		case "AND", "&&":
			if !total {
				return total, nil
			}
			total = total && current
		case "OR", "||":
			total = total || current
		}
	}
	return total, nil
}

//bind functions
func binds(variables *map[string]interface{}) (b map[string]func(params ...interface{}) interface{}) {
	b = make(map[string]func(params ...interface{}) interface{})
	b["isprime"] = func(params ...interface{}) interface{} {
		if len(params) < 1 {
			return false
		}
		n, _ := strconv.Atoi(fmt.Sprint(params[0]))
		for i := 2; i < n; i++ {
			if n%i == 0 {
				return false
			}
		}
		return true
	}
	b["iseven"] = func(params ...interface{}) interface{} {
		if len(params) < 1 {
			return false
		}
		n, _ := strconv.Atoi(fmt.Sprint(params[0]))
		return n%2 == 0
	}
	b["divisible"] = func(params ...interface{}) interface{} {
		if len(params) < 2 {
			return false
		}
		f1, _ := strconv.Atoi(fmt.Sprint(params[0]))
		f2, _ := strconv.Atoi(fmt.Sprint(params[1]))
		return f1%f2 == 0
	}
	b["add"] = func(params ...interface{}) interface{} {
		if len(params) < 2 {
			return params[0]
		}
		sum := float64(0)
		for _, param := range params {
			n, _ := strconv.ParseFloat(fmt.Sprint(param), 64)
			sum += n
		}
		return sum
	}
	b["plus"] = b["add"]
	b["subtract"] = func(params ...interface{}) interface{} {
		if len(params) < 2 {
			return params[0]
		}
		n1, _ := strconv.ParseFloat(fmt.Sprint(params[0]), 64)
		n2, _ := strconv.ParseFloat(fmt.Sprint(params[1]), 64)
		return n1 - n2
	}
	b["minus"] = b["subtract"]
	b["divide"] = func(params ...interface{}) interface{} {
		if len(params) < 2 {
			return params[0]
		}
		f1, _ := strconv.ParseFloat(fmt.Sprint(params[0]), 64)
		f2, _ := strconv.ParseFloat(fmt.Sprint(params[1]), 64)
		return f1 / f2
	}
	b["multiply"] = func(params ...interface{}) interface{} {
		if len(params) < 2 {
			return params[0]
		}
		f1, _ := strconv.Atoi(fmt.Sprint(params[0]))
		f2, _ := strconv.Atoi(fmt.Sprint(params[1]))
		return f1 * f2
	}
	b["random"] = func(params ...interface{}) interface{} {
		/**
		* rand min max condition
		* condition is optional and can be prime,even,odd
		**/
		if len(params) < 2 {
			max, _ := strconv.Atoi(fmt.Sprint(params[0]))
			return rand.Intn(max)
		}
		min, _ := strconv.Atoi(fmt.Sprint(params[0]))
		max, _ := strconv.Atoi(fmt.Sprint(params[1]))
		val := rand.Intn(max-min) + min
		if len(params) == 3 {
			switch fmt.Sprint(params[2]) {
			case "even":
				if val%2 != 0 {
					//convert to even by multiply by 2
					val = val * 2
				}
			case "odd":
				if val%2 == 0 {
					//convert to odding 1
					val++
				}
				//impliment for prime later
			}
		}
		return val
	}
	return b
}

//parse function parameters or values
func parseParams(variables *map[string]interface{}, params ...interface{}) (parsed []interface{}) {
	parsed = make([]interface{}, 0)
	//check if min is a compiled variable
	for _, param := range params {
		//is it in variable format and does that variable exist?
		match := varFuncSliceRegx.FindStringSubmatch(fmt.Sprint(param))
		if match[varFuncSliceRegxMap["variable"]] != "" { //it a function
			name := match[varFuncSliceRegxMap["vname"]]
			if _val, ok := (*variables)[name]; ok {
				parsed = append(parsed, _val)
				continue
			}
		}
		//check if its a float
		if regexp.MustCompile(`[0-9]+\.[0-9]+`).MatchString(fmt.Sprint(param)) {
			p, _ := strconv.ParseFloat(fmt.Sprint(param), 32)
			parsed = append(parsed, p)
			continue
		}
		//try to parse it as int
		p, err := strconv.Atoi(fmt.Sprint(param))
		if err == nil { //if no error then we are good for int
			parsed = append(parsed, p)
			continue
		}

		//make a stirng since above did not work
		parsed = append(parsed, fmt.Sprint(param))
	}
	return parsed
}

//map regex expression names
func mapRegxNames(list []string) (exMap map[string]int) {
	exMap = make(map[string]int)
	for i, v := range list {
		exMap[v] = i
	}
	return exMap
}

//this will change the existing template
// func (h *progTemplate) ParseTemplate(template string, strt templateStructure) {
// 	indexes := baseRegx.FindAllStringIndex(template, -1)
// 	baseRegxMatches := baseRegx.FindAllStringSubmatch(template, -1)
// 	//structure the template
// 	h.template = template
// 	h.structures, _ = buildStructure(indexes, baseRegxMatches, baseRegxMap)
// }

//process the structured template
func (h *progTemplate) process(template string, strt templateStructure, offset int) (string, error) {
	if strt.match[baseRegxMap["end"]] != "" { //remove the end tag
		template = strings.Replace(template, strt.match[baseRegxMap[""]], "", 1)
	}
	if strt.match[baseRegxMap["show"]] != "" { //render a variable
		_v := parseParams(h.globals, strt.match[baseRegxMap["vname"]])
		template = strings.Replace(template, strt.match[baseRegxMap[""]], fmt.Sprint(_v[0]), 1)
		//update template
		// template = template[:strt.index[0]+offset] + fmt.Sprint(_v[0]) + template[strt.index[1]+offset:]
	}
	if strt.match[baseRegxMap["declarer"]] != "" { //run declearation
		//match the value so as to know if its a variable or function assignment
		valueMatch := varFuncSliceRegx.FindStringSubmatch(strt.match[baseRegxMap["value"]])
		if valueMatch[varFuncSliceRegxMap["slice"]] != "" { //its a variable
			//get the parameters and convert it to a slice of interface{}
			_p := strings.Split(strings.TrimSpace(valueMatch[varFuncSliceRegxMap["svalue"]]), " ")
			params := make([]interface{}, len(_p))
			for i, p := range _p {
				params[i] = p
			}
			switch strt.match[baseRegxMap["operator"]] {
			case "=", ":=":
				(*h.globals)[strt.match[baseRegxMap["name"]]] = parseParams(h.globals, params...)
			}
		}
		if valueMatch[varFuncSliceRegxMap["variable"]] != "" { //its a variable
			//pass the value(vname) to see it it exists or make it
			_v := parseParams(h.globals, valueMatch[varFuncSliceRegxMap["vname"]])
			// fmt.Println(_v)
			//check if the proccess was an assignment or initialization
			switch strt.match[baseRegxMap["operator"]] {
			case "=", ":=":
				(*h.globals)[strt.match[baseRegxMap["name"]]] = _v[0]
			}
		}
		if valueMatch[varFuncSliceRegxMap["function"]] != "" { //its a function assignment
			var (
				bind func(params ...interface{}) interface{}
				ok   bool
			)
			//check if the binder function exists
			if bind, ok = binds(h.globals)[valueMatch[varFuncSliceRegxMap["fname"]]]; !ok {
				return template, fmt.Errorf("invalid for loop evaluator")
			}
			//get the parameters and convert it to a slice of interface{}
			_p := strings.Split(strings.TrimSpace(valueMatch[varFuncSliceRegxMap["params"]]), " ")
			params := make([]interface{}, len(_p))
			for i, p := range _p {
				params[i] = p
			}
			(*h.globals)[strt.match[baseRegxMap["name"]]] = bind(parseParams(h.globals, params...)...)
		}
		template = strings.Replace(template, strt.match[baseRegxMap[""]], "", 1)
		//update template
		// template = template[:strt.index[0]+offset] + template[strt.index[1]+offset:]
	}
	//proccess blocks
	if strt.match[baseRegxMap["block"]] != "" { //run th block
		conditionTemplate := strt.match[baseRegxMap["condition"]] //get condition template
		//its a block
		switch strt.match[baseRegxMap["type"]] { //run bases on type
		case "if":
			b, err := h.validateCondition(ifforRegx.FindAllStringSubmatch(conditionTemplate, -1), ifforRegxMap)
			if err != nil {
				return template, err
			}
			body := template[strt.index[1]+offset : strt.children[len(strt.children)-1].index[1]+offset]
			_body := body
			if b { //execute children if true
				//run the children
				for _, structure := range strt.children {
					//for now return the template
					body, err = h.process(body, structure, offset)
					if err != nil {
						return template, err
					}
				}
				//remove body
				template = strings.Replace(template, _body, body, 1)
			} else {
				//remove body
				template = strings.Replace(template, _body, "", 1)
			}
			//remove the condition
			template = strings.Replace(template, strt.match[baseRegxMap[""]], "", 1)
		case "for":
			//get body of the block
			body := template[strt.index[1]+offset : strt.children[len(strt.children)-1].index[1]+offset]
			bodyBatch := "" //the batch of processed body
			// offset = offset - (strt.index[1] - strt.index[0])
			//run the content
			for {
				t, err := h.validateCondition(ifforRegx.FindAllStringSubmatch(conditionTemplate, -1), ifforRegxMap)
				if err != nil {
					return template, err
				}
				if !t { //break once the condtion fails
					break
				}
				b := body
				//run the children
				for _, structure := range strt.children {
					b, err = h.process(b, structure, 0)
					if err != nil {
						return template, err
					}
				}
				bodyBatch += b
			}
			//remove the loop condition
			template = strings.Replace(template, strt.match[baseRegxMap[""]], "", 1)
			//remove body
			template = strings.Replace(template, body, bodyBatch, 1)
			//push in the content
			// template = template[:strt.index[1]+offset] + bodyBatch + template[strt.children[len(strt.children)-1].index[1]+offset:]
		}
	}
	//trim before return
	return strings.TrimSpace(template), nil
}

//returns the variable in the template engine
func (h *progTemplate) Variables() map[string]interface{} {
	return (*h.globals)
}

func (h *progTemplate) Execute() (curTemplate string, err error) {
	//process the structure
	template := h.template
	// template = strings.ReplaceAll(template, "\n", " ")
	// template = strings.ReplaceAll(template, "\t", " ")
	curTemplate = template
	for _, structure := range h.structures {
		offset := (len(curTemplate) - len(template))
		curTemplate, err = h.process(curTemplate, structure, offset)
		if err != nil {
			return curTemplate, err
		}
	}
	return curTemplate, nil
}
